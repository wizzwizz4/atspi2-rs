use core::mem::drop;
use dbus_codegen::{
    ConnectionType,
    generate,
    GenOpts,
    ServerAccess
};
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

fn main() {
    let out_dir: PathBuf = env::var_os("OUT_DIR").unwrap().into();

    ////
    // Objects and interfaces

    let xml_dir = Path::new("at-spi2-core/xml");
    println!("cargo:rerun-if-changed=at-spi2-core/xml/Accessibility.xml");
    let index = include_str!("at-spi2-core/xml/Accessibility.xml")
        .lines()
        .skip(2)
        .filter(|line| *line != "</spec>")
        .map(|line| line.split('"').skip(1).next().unwrap())

        .chain([
            "Event.xml",
            "Socket.xml",
        ]);

    let mut f = File::create(out_dir.join("dbus_bindings.rs")).unwrap();

    for filename in index {
        let xml_data = {
            let mut s = String::new();
            File::open(xml_dir.join(filename)).unwrap()
                .read_to_string(&mut s).unwrap();
            s
        };
        eprintln!("Generating {}", filename);
        println!("cargo:rerun-if-changed=at-spi2-core/xml/{}", filename);
        let opts = GenOpts {
            crhandler: None,
            skipprefix: Some("org.a11y.atspi".into()),
            connectiontype: ConnectionType::Nonblock,
            propnewtype: true,
            ..Default::default()
        };
        let server_code = generate(&xml_data, &opts).unwrap();
        let client_code = generate(&xml_data, &GenOpts {
            methodtype: None,
            ..opts
        }).unwrap();

        let mod_name = filename.split('.').next().unwrap().to_lowercase();
        write!(f, "pub mod {} {{\n", mod_name).unwrap();
        write!(f, "pub mod server {{\n").unwrap();
        f.write(server_code.as_bytes()).unwrap();
        write!(f, "}}\n").unwrap();
        write!(f, "\npub mod client {{\n").unwrap();
        f.write(client_code.as_bytes()).unwrap();
        write!(f, "}}\n}}\n").unwrap();
    }

    drop(f);

    ////
    // Constants

    println!("cargo:rerun-if-changed=at-spi2-core/atspi/atspi-constants.h");
    let source = include_str!("at-spi2-core/atspi/atspi-constants.h");

    let mut f = File::create(out_dir.join("atspi-constants-header.md"))
        .unwrap();

    let mut lines = source.lines();
    assert_eq!(lines.next(), Some("/*"));
    for line in &mut lines {
        // Title
        if line == " *" { break };
        writeln!(f, "{}  ", line.strip_prefix(" * ").unwrap()).unwrap();
    }
    writeln!(f, "").unwrap();
    for line in &mut lines {
        // Copyright
        if line == " *" { break };
        writeln!(f, " {}", line).unwrap();  // conveniently starts with " *"
    }
    writeln!(f, "  * Copyright 2021 wizzwizz4").unwrap();
    writeln!(f, "").unwrap();
    writeln!(f, "---").unwrap();
    writeln!(f, "").unwrap();
    for line in &mut lines {
        // LGPL-2.1-or-later
        if line == " */" { break };
        writeln!(
            f, "{}",
            line.strip_prefix(" *").unwrap().trim_start()
        ).unwrap();
    }
    writeln!(f, "").unwrap();
    writeln!(f, "---").unwrap();
    writeln!(f, "").unwrap();
    assert_eq!(lines.next(), Some(""));
    assert_eq!(lines.next(), Some("/* TODO: Auto-generate this file again"));
    let mut in_list_item = false;
    for line in &mut lines {
        if line == "" {
            in_list_item = false;
            continue;
        }
        if line == " */" { break };

        if in_list_item {
            if line.starts_with("  Accessibility::") {
                in_list_item = false;
            } else {
                // conveniently starts with "  "
                writeln!(f, "  {}", line.trim_end_matches(r"\n")).unwrap();
                continue;
            }
        }

        if let Some(title) = line.strip_prefix(r" !\mainpage ") {
            writeln!(f, "\n# {}", title).unwrap();
        } else if let Some(section) = line.strip_prefix(r"  \section ") {
            let section = match section.split_once(' ') {
                Some((_name, section)) => section,
                None => section
            };
            writeln!(f, "\n## {}", section).unwrap();
        } else if line.starts_with("  Accessibility::") {
            let line = line.trim_start().trim_end_matches(r"\n").trim_end();
            if let Some((interface, info)) = line.split_once(' ') {
                writeln!(f, "  * `{}` {}", interface, info).unwrap();
                in_list_item = true;
            } else {
                writeln!(f, "  * `{}`", line).unwrap();
            }
        } else {
            let extra_line = line.ends_with(r"\n");
            let line = line.trim_start().trim_end_matches(r"\n").trim_end();
            writeln!(f, "{}", line).unwrap();
            if extra_line {
                writeln!(f, "").unwrap();
            }
        }
    }
    assert_eq!(lines.next(), Some(""));
    drop(f);

    let mut f = File::create(out_dir.join("atspi-constants.rs")).unwrap();
    assert_eq!(lines.nth(5), Some(""));  // #pragma once etc.

    // Enums
    let mut line = "";
    loop {
        line = lines.next().unwrap();
        if line == "" { continue };
        if line.starts_with("#define") { break };

        let mut docs = HashMap::new();
        let type_name;
        if line == "/**" {
            type_name = lines.next().unwrap()
                .strip_prefix(" *").unwrap()
                .trim_start()
                .strip_suffix(":").unwrap();
            let mut variant = None;
            let mut doc = Vec::new();
            for line in &mut lines {
                if line == " *" { break };
                if line == "" { continue };
                if let Some(line) =
                    line.strip_prefix(" *")
                        .map(|line| line.trim_start())
                        .and_then(|line| line.strip_prefix("@"))
                        .and_then(|line| if line.contains(": ") {
                            Some(line)
                        } else {
                            None
                        })
                {
                    let (var, line) = line.split_once(": ").unwrap();
                    if let Some(var) = variant {
                        docs.insert(var, doc);
                    }
                    doc = Vec::new();
                    variant = Some(var);
                    doc.push(line);
                } else {
                    doc.push(line.strip_prefix(" *").unwrap().trim_start());
                }
            }
            docs.insert(variant.unwrap(), doc);
            for line in &mut lines {
                if line == " *" {
                    writeln!(f, "///").unwrap();
                    continue;
                };
                if line == " */" || line == " **/" { break };
                writeln!(f, "/// {}", line.strip_prefix(" * ").unwrap())
                    .unwrap();
            }
            assert_eq!(lines.next(), Some("typedef enum {"));
        } else {
            type_name = "AtspiCache";
            let mut stage = 0;
            loop {
                for word in line.split_whitespace() {
                    eprintln!("{}", line);
                    stage = match stage {
                        0 => { assert_eq!(word, "typedef"); 1 },
                        1 => { assert_eq!(word, "enum"); 2 },
                        2 => { assert_eq!(word, "{"); 3 },
                        _ => panic!(),
                    };
                }
                if stage == 3 {
                    break;
                }
                line = lines.next().unwrap();
            }
        }
        writeln!(f, "pub struct {}(pub u32);", type_name).unwrap();
        writeln!(f, "impl {} {{", type_name).unwrap();
        let mut counter = 0;
        let mut values = HashMap::new();
        for line in &mut lines {
            if line.starts_with("} ") {
                let line = line.strip_prefix("} ").unwrap();
                let line = line.strip_suffix(";").unwrap();
                assert_eq!(line, type_name);
                break;
            }
            let line = line.trim_start();
            let line = line.strip_suffix(",").unwrap_or(line);
            let (var, expr) = if let Some((var, expr)) = line.split_once(" = ") {
                if let Ok(i) = expr.parse::<u32>() {
                    // decimal
                    counter = i;
                    (var, Some(expr))
                } else if let Some((a, b)) = expr.split_once(" << ") {
                    // decimal << decimal
                    let a = a.parse::<u32>().unwrap();
                    let b = b.parse::<u32>().unwrap();
                    counter = a << b;
                    (var, Some(expr))
                } else if let Some(hex) = expr.strip_prefix("0x") {
                    // hex
                    counter = u32::from_str_radix(hex, 16).unwrap();
                    (var, Some(expr))
                } else {
                    // previous enum variants separated by |
                    let mut total = 0;
                    for other_var in expr.split(" | ") {
                        total |= values[other_var];
                    }
                    counter = total;
                    (var, None)
                }
            } else {
                (line, None)
            };
            let var = var.trim_end();
            if !docs.is_empty() {
                for line in &docs[var] {
                    writeln!(f, "/// {}", line).unwrap();
                }
            }
            if let Some(expr) = expr {
                writeln!(f, "pub const {}: u32 = {};", var, expr).unwrap();
            } else {
                writeln!(f, "pub const {}: u32 = {};", var, counter).unwrap();
            }
            values.insert(var, counter);
            counter += 1;
        }
        writeln!(f, "}}").unwrap();
        writeln!(f, "").unwrap();
        if !["AtspiTextBoundaryType", "AtspiCache"].contains(&type_name) {
            assert_eq!(lines.next(), Some(""));
            assert_eq!(lines.next(), Some("/**"));
            for line in &mut lines {
                if line == " */" || line == " **/" { break };
            }
            assert!(lines.next().unwrap().starts_with("#define ATSPI_"));
        }
    }

    // Defines
    while line != "#ifdef __cplusplus" {
        if line != "" {
            line = line.strip_prefix("#define ").unwrap();
            let (name, expr) = line.split_once(' ').unwrap();
            writeln!(f, "pub const {}: &'static str = {};", name, expr)
                .unwrap();
        }
        line = lines.next().unwrap();
    }

    assert_eq!(lines.nth(3), None);
}
