//! # AT-SPI2 interface for Rust
//! ## Getting started
//! Are you making a user interface or an assistive tool?
//!
//! ### User interface
//! Implement the `interfaces` for your widgets as semantically appropriate.
//! Caution: nobody's actually sure what the proper semantics are.
//! Since only Orca really uses AT-SPI2,
//! I recommend just doing what works in Orca.
//! (The documented semantics and the Best Practices guide take precedence
//!  over "what works in Orca", but they're not complete.)
//!
//! Once you've implemented the interfaces, construct a D-Bus object tree?
//! TODO: work out how this works.
//!
//! Also, do events, and caching.

//! ### Assistive tool
//! This is much easier. Open a connection to the accessibility bus,
//! navigate it, then use the traits.
//!
//! ## Useful links
//!   * [New GNOME accessibility guidelines](https://developer.gnome.org/documentation/guidelines/accessibility.html)
//!   * [ATK/AT-SPI Best Practices Guide 2011 DRAFT](https://wiki.gnome.org/Accessibility/ATK/BestPractices)
//!   * [AT-SPI2 semantics](https://accessibility.linuxfoundation.org/a11yspecs/atspi/adoc/ADOC_ATSPI.html)
//!   * [D-Bus specification](https://dbus.freedesktop.org/doc/dbus-specification.html)

/// Generated bindings.
///
/// Documented even worse than the schemas they're generated from.
/// Only useful if you already understand AT-SPI2.
/// Each module is generated from the XML file with the same name.
#[allow(unused_variables)]
mod bindings {
    include!(concat!(env!("OUT_DIR"), "/dbus_bindings.rs"));
}

/// Generated constants.
///
/// Actually documented quite well.
///
#[doc = include_str!(concat!(env!("OUT_DIR"), "/atspi-constants-header.md"))]
#[allow(non_snake_case)]
#[allow(non_upper_case_globals)]
pub mod constants {
    include!(concat!(env!("OUT_DIR"), "/atspi-constants.rs"));

    use core::ops::{
        BitAnd, BitAndAssign,
        BitOr, BitOrAssign,
    };
    use self::AtspiCache as C;
    impl BitAnd<C> for C {
        type Output = C;
        #[inline] fn bitand(self, o: C) -> C { C(self.0 & o.0) }
    }
    impl BitAndAssign<C> for C {
        #[inline] fn bitand_assign(&mut self, o: C) { self.0 &= o.0 }
    }
    impl BitOr<C> for C {
        type Output = C;
        #[inline] fn bitor(self, o: C) -> C { C(self.0 | o.0) }
    }
    impl BitOrAssign<C> for C {
        #[inline] fn bitor_assign(&mut self, o: C) { self.0 |= o.0 }
    }
}

pub mod interfaces {
    pub mod client {
        pub use crate::bindings::accessible::client::Accessible;
        pub use crate::bindings::action::client::Action;
        pub use crate::bindings::application::client::Application;
        pub use crate::bindings::collection::client::Collection;
        pub use crate::bindings::component::client::Component;
        pub use crate::bindings::document::client::Document;
        pub use crate::bindings::hypertext::client::Hypertext;
        pub use crate::bindings::hyperlink::client::Hyperlink;
        pub use crate::bindings::image::client::Image;
        pub use crate::bindings::selection::client::Selection;
        pub use crate::bindings::table::client::Table;
        pub use crate::bindings::tablecell::client::TableCell;
        pub use crate::bindings::text::client::Text;
        pub use crate::bindings::editabletext::client::EditableText;
        pub use crate::bindings::value::client::Value;
    }
}

pub mod events {
    use crate::bindings::registry;
}

///
pub mod bus {
}
